#include <RecalboxConf.h>
#include <guis/GuiNetPlay.h>
#include <systems/SystemManager.h>
#include "guis/GuiGamelistOptions.h"
#include "views/gamelist/ISimpleGameListView.h"
#include "systems/SystemData.h"
#include "Window.h"
#include "views/ViewController.h"
#include "Settings.h"
#include "utils/locale/LocaleHelper.h"

ISimpleGameListView::ISimpleGameListView(Window& window, SystemManager& systemManager, SystemData& system)
  : IGameListView(window, system),
    mSystemManager(systemManager),
    mHeaderText(window),
    mHeaderImage(window),
    mBackground(window),
    mThemeExtras(window)
{
  int favoritesCount = system.FavoritesCount();
  mFavoritesOnly = favoritesCount > 0 && Settings::Instance().FavoritesOnly();

  mHeaderText.setText("Logo Text");
  mHeaderText.setSize(mSize.x(), 0);
  mHeaderText.setPosition(0, 0);
  mHeaderText.setHorizontalAlignment(TextAlignment::Center);
  mHeaderText.setDefaultZIndex(50);

  mHeaderImage.setResize(0, mSize.y() * 0.185f);
  mHeaderImage.setOrigin(0.5f, 0.0f);
  mHeaderImage.setPosition(mSize.x() / 2, 0);
  mHeaderImage.setDefaultZIndex(50);

  mBackground.setResize(mSize.x(), mSize.y());
  mBackground.setDefaultZIndex(0);

  addChild(&mHeaderText);
  addChild(&mBackground);
}

void ISimpleGameListView::onThemeChanged(const ThemeData& theme)
{
  mBackground.applyTheme(theme, getName(), "background", ThemeProperties::All);
  mHeaderImage.applyTheme(theme, getName(), "logo", ThemeProperties::All);
  mHeaderText.applyTheme(theme, getName(), "logoText", ThemeProperties::All);

  // Remove old theme extras
  for (auto extra : mThemeExtras.getmExtras()) {
    removeChild(extra);
  }
  mThemeExtras.getmExtras().clear();

  mThemeExtras.setExtras(ThemeData::makeExtras(theme, getName(), mWindow));
  mThemeExtras.sortExtrasByZIndex();

  // Add new theme extras

  for (auto extra : mThemeExtras.getmExtras()) {
    addChild(extra);
  }


  if (mHeaderImage.hasImage()) {
    removeChild(&mHeaderText);
    addChild(&mHeaderImage);
  } else {
    addChild(&mHeaderText);
    removeChild(&mHeaderImage);
  }
}

void ISimpleGameListView::onFileChanged(FileData* file, FileChangeType change)
{
  if (change == FileChangeType::Run)
  {
    updateInfoPanel();
    return ;
  }

  if ((change == FileChangeType::Removed) && (file == getEmptyListItem()))
    return;

  if (file->isGame())
  {
    SystemData* favoriteSystem = mSystemManager.FavoriteSystem();
    bool isInFavorite = favoriteSystem->getRootFolder().Contains(file, true);
    bool isFavorite = file->Metadata().Favorite();

    if (isInFavorite != isFavorite)
    {
      if (isInFavorite) favoriteSystem->getRootFolder().removeChild(file);
      else favoriteSystem->getRootFolder().addChild(file, false);
      ViewController::Instance().setInvalidGamesList(mSystemManager.FavoriteSystem());
      ViewController::Instance().getSystemListView().manageFavorite();
      if (mSystem.FavoritesCount() == 0) mFavoritesOnly = false;
      updateHelpPrompts();
    }
  }

  if (change == FileChangeType::Removed)
  {
    bool favorite = file->Metadata().Favorite();
    delete file;
    if (favorite)
    {
      ViewController::Instance().setInvalidGamesList(mSystemManager.FavoriteSystem());
      ViewController::Instance().getSystemListView().manageFavorite();
    }
  }

  int cursor = getCursorIndex();
  if (RecalboxConf::Instance().AsBool(mSystem.getName() + ".flatfolder"))
  {
    populateList(mSystem.getRootFolder());
  }
  else
  {
    refreshList();
  }
  setCursorIndex(cursor);

  updateInfoPanel();
}

bool ISimpleGameListView::ProcessInput(const InputCompactEvent& event) {
  bool hideSystemView = RecalboxConf::Instance().AsBool("emulationstation.hidesystemview");

  // RUN GAME or ENTER FOLDER
  if (event.BPressed())
  {
    FileData* cursor = getCursor();
    if (cursor->isGame())
    {
      //Sound::getFromTheme(getTheme(), getName(), "launch")->play();
      launch(cursor);
    }
    else if (cursor->isFolder())
    {
      FolderData* folder = (FolderData*)cursor;
      if (folder->hasChildren())
      {
        mCursorStack.push(folder);
        populateList(*folder);
        setCursorIndex(0);
      }
    }
    RecalboxSystem::NotifyGame(*getCursor(), false, false);
    return true;
  }

  // BACK to PARENT FOLDER or PARENT SYSTEM
  if (event.APressed())
  {
    if (!mCursorStack.empty())
    {
      FolderData* selected = mCursorStack.top();

      // remove current folder from stack
      mCursorStack.pop();

      FolderData& cursor = !mCursorStack.empty() ? *mCursorStack.top() : mSystem.getRootFolder();
      populateList(cursor);

      setCursor(selected);
      //Sound::getFromTheme(getTheme(), getName(), "back")->play();
      RecalboxSystem::NotifyGame(*getCursor(), false, false);
    }
    else if (!hideSystemView)
    {
      onFocusLost();
      ViewController::Instance().goToSystemView(&mSystem);
    }
    return true;
  }

  // TOGGLE FAVORITES
  if (event.YPressed())
  {
    FileData* cursor = getCursor();
    if (cursor->isGame() && cursor->getSystem()->getHasFavoritesInTheme())
    {
      MetadataDescriptor& md = cursor->Metadata();
      SystemData *favoriteSystem = mSystemManager.FavoriteSystem();

      md.SetFavorite(!md.Favorite());

      if (favoriteSystem != nullptr)
      {
        if (md.Favorite()) favoriteSystem->getRootFolder().addChild(cursor, false);
        else               favoriteSystem->getRootFolder().removeChild(cursor);

        ViewController::Instance().setInvalidGamesList(cursor->getSystem());
        ViewController::Instance().setInvalidGamesList(favoriteSystem);
      }
      ViewController::Instance().getSystemListView().manageFavorite();

      // Reset favorite-only view if no more favorites
      if (mSystem.FavoritesCount() == 0) mFavoritesOnly = false;

      // Reload to refresh the favorite icon
      int cursorPlace = getCursorIndex();
      refreshList();
      setCursorIndex(cursorPlace);

      updateHelpPrompts();
    }
    RecalboxSystem::NotifyGame(*getCursor(), false, false);
    return true;
  }

  // MOVE to NEXT GAMELIST
  if (event.AnyRightPressed())
  {
    if (Settings::Instance().QuickSystemSelect() && !hideSystemView) {
      onFocusLost();
      ViewController::Instance().goToNextGameList();
      RecalboxSystem::NotifyGame(*getCursor(), false, false);
      return true;
    }
  }

  // MOVE to PREVIOUS GAMELIST
  if (event.AnyLeftPressed()) {
    if (Settings::Instance().QuickSystemSelect() && !hideSystemView) {
      onFocusLost();
      ViewController::Instance().goToPrevGameList();
      RecalboxSystem::NotifyGame(*getCursor(), false, false);
      return true;
    }
  }


  // JUMP TO NEXT LETTER
  if (event.L1Pressed())
  {
    jumpToNextLetter(mSystem.getSortId() == 1);
    return true;
  }

  // JUMP TO PREVIOUS LETTER
  if (event.R1Pressed())
  {
      jumpToNextLetter(mSystem.getSortId() == 1);
      return true;
  }

  // JUMP TO -10
  if (event.L2Pressed())
  {
    auto index = getCursorIndex();
    if (index > 0)
      setCursorIndex(index > 10 ? index - 10 : 0);
    else
      setCursorIndex(getCursorIndexMax());
    return true;
  }

  // JUMP TO +10
  if (event.R2Pressed())
  {
    auto index = getCursorIndex();
    auto max = getCursorIndexMax();
    if (index == max)
      setCursorIndex(0);
    else
      setCursorIndex(index > max - 10 ? max : index + 10);
    return true;
  }

  // NETPLAY
  if ((event.XPressed()) && (RecalboxConf::Instance().AsBool("global.netplay"))
      && (RecalboxConf::Instance().isInList("global.netplay.systems", getCursor()->getSystem()->getName())))
  {
    FileData* cursor = getCursor();
    if(cursor->isGame())
    {
      Vector3f target(Renderer::getDisplayWidthAsFloat() / 2.0f, Renderer::getDisplayHeightAsFloat() / 2.0f, 0);
      ViewController::Instance().launch(cursor, target, "host");
    }
  }

  if (event.StartPressed())
  {
    mWindow.pushGui(new GuiGamelistOptions(mWindow, &mSystem));
    return true;
  }

  if (event.SelectPressed() && !IsFavoriteSystem())
  {
    if (mSystem.FavoritesCount() != 0)
    {
      mFavoritesOnly = !mFavoritesOnly;
      Settings::Instance().SetFavoritesOnly(mFavoritesOnly);
      refreshList();
      updateInfoPanel();
      updateHelpPrompts();
    }
    return true;
  }

  bool result = IGameListView::ProcessInput(event);

  if (event.AnythingPressed())
      RecalboxSystem::NotifyGame(*getCursor(), false, false);

  return result;
}

bool ISimpleGameListView::getHelpPrompts(Help& help)
{
  bool hideSystemView = RecalboxConf::Instance().AsBool("emulationstation.hidesystemview");

  help.Set(HelpType::B, _("LAUNCH"));

  if (RecalboxConf::Instance().AsBool("global.netplay") && (RecalboxConf::Instance().isInList("global.netplay.systems", getCursor()->getSystem()->getName())))
    help.Set(HelpType::X, _("NETPLAY"));

  help.Set(HelpType::Y, IsFavoriteSystem() ? _("Remove from favorite") : _("Favorite"));

  if (!hideSystemView)
    help.Set(HelpType::A, _("BACK"));

  help.Set(HelpType::UpDown, _("CHOOSE"));

  if (Settings::Instance().QuickSystemSelect() && !hideSystemView)
    help.Set(HelpType::LeftRight, _("SYSTEM"));

  if (!IsFavoriteSystem())
  {
    help.Set(HelpType::Start, _("OPTIONS"));
    if (mSystem.FavoritesCount() != 0)
      help.Set(HelpType::Select, mFavoritesOnly ? _("ALL GAMES") : _("FAVORITES ONLY"));
  }

  return true;
}

std::string ISimpleGameListView::getAvailableLetters()
{
  constexpr int UnicodeSize = 0x10000;
  FileData::List files = getFileDataList(); // All file
  std::vector<unsigned int> unicode;        // 1 bit per unicode char used
  unicode.resize(UnicodeSize / (sizeof(unsigned int) * 8), 0);

  for (auto file : files)
  {
    // Tag every first characters from every game name
    unsigned int wc = Strings::UpperChar(file->getName());
    if (wc < UnicodeSize) // Ignore extended unicodes
      unicode[wc >> 5] |= 1 << (wc & 0x1F);
  }

  // Rebuild a self-sorted utf8 string with all tagged characters
  int unicodeOffset = 0;
  std::string result;
  for(unsigned int i : unicode)
  {
    if (i != 0)
      for (int bit = 0; bit < 32; ++bit)
        if (((i >> bit) & 1) != 0)
          result.append(Strings::unicode2Chars(unicodeOffset + bit));
    unicodeOffset += 32;
  }

  return result;
}


void ISimpleGameListView::jumpToNextLetter(bool forward)
{
  // Get current unicode
  unsigned int currentUnicode = Strings::UpperChar(getCursor()->getName());

  // Get available unicodes
  std::string letters = getAvailableLetters();
  std::vector<unsigned int> availableUnicodes;
  int position = 0;
  while(position < (int)letters.size())
    availableUnicodes.push_back(Strings::UpperChar(Strings::chars2Unicode(letters, position)));

  // Lookup current unicode
  position = 0;
  for(int i = (int)availableUnicodes.size(); --i >= 0;)
    if (availableUnicodes[i] == currentUnicode)
    {
      position = i;
      break;
    }

  int size = (int)availableUnicodes.size();
  jumpToLetter(availableUnicodes[(size + position + (forward ? 1 : -1)) % size]);
}

void ISimpleGameListView::jumpToLetter(unsigned unicode)
{
  // Jump to letter requires an alpha sort
  if ( mSystem.getSortId() > 1) {
    // apply sort
    mSystem.setSortId(0);
    // notify that the root folder has to be sorted
    onFileChanged(&mSystem.getRootFolder(), FileChangeType::Sorted);
  }

  FileData::List files = getFileDataList();

  unsigned long min, max;
  unsigned long mid = 0;

  bool asc = (mSystem.getSortId() == 0);

  // look for first game position
  for (min = 0; (min < files.size() - 1) && (files[min]->getType() != ItemType::Game) ; min++) ;

  // look for last game position
  for (max = files.size() - 1; (max != 0u) && (files[max]->getType() != ItemType::Game) ; max--) ;

  while(max >= min)
  {
    mid = ((max - min) / 2) + min;

    // game somehow has no first character to check
    if (files[mid]->getName().empty()) continue;

    unsigned int checkLetter = Strings::UpperChar(files[mid]->getName());

    if (asc)
    {
      if (checkLetter < unicode) min = mid + 1;
      else if (checkLetter > unicode || (mid > 0 && (unicode == Strings::UpperChar(files[mid - 1]->getName())))) max = mid - 1;
      else break; //exact match found
    }
    else
    {
      if (checkLetter > unicode) min = mid + 1;
      else if (checkLetter < unicode || (mid > 0 && (unicode == Strings::UpperChar(files[mid - 1]->getName())))) max = mid - 1;
      else break; //exact match found
    }
  }

  setCursor(files[mid]);
}